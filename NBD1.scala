object Main {
  def main(args: Array[String]): Unit = {
    val days = List(
      "Poniedziałek",
      "Wtorek",
      "Środa",
      "Czwartek",
      "Piątek",
      "Sobota",
      "Niedziela"
    )

    val games = Map(
      "Biomutant" -> 249.99,
      "Outriders" -> 249.00,
      "Serious Sam 4" -> 142.99,
      "Carrion" -> 71.99,
      "Half-Life Alyx" -> 214.99
    )

    val myTuple = Tuple3("Adam", 18, 195.5)

    val summits = Map(
      "Asia" -> "Mount Everest",
      "South America" -> "Aconcagua",
      "North America" -> "Denali",
      "Africa" -> "Kilimanjaro",
      "Antarctica" -> "Vinson Massif"
    )

    val intList = List(0, 1, 2, 3, 4, 0, 5, 6, 7, 0, 8, 9)
    val realNumbers = List(-8, -5, 22, 12, 11, 5, 6, -1, 3, 55, -100)

    println("zad1")
    println("-------------------------")
    daysOfTheWeek(days)

    println("\nzad2")
    println("-------------------------")
    println(daysOfTheWeekRec(days))
    println(daysOfTheWeekRecReverse(days))

    println("\nzad3")
    println("-------------------------")
    println(daysOfTheWeekTailrec(days))

    println("\nzad4")
    println("-------------------------")
    daysOfTheWeekFold(days)

    println("\nzad5")
    println("-------------------------")
    println(makeDiscount(games));

    println("\nzad6")
    println("-------------------------")
    printTuple(myTuple)

    println("\nzad7")
    println("-------------------------")
    sevenSummits(summits)

    println("\nzad8")
    println("-------------------------")
    println(removeZero(intList))

    println("\nzad9")
    println("-------------------------")
    println(mapList(intList))

    println("\nzad10")
    println("-------------------------")
    println(filterList(realNumbers))

  }

  //1
  def daysOfTheWeek(days: List[String]) = {
    var output1 = ""

    for (day <- days) {
      output1 = output1.concat(day + ", ")
    }

    println(output1)

    var output2 = ""

    for (day <- days if day.startsWith("P")) {
      output2 = output2.concat(day + ", ")
    }
    println(output2)

    var output3 = ""
    var iter = 0

    while (iter < days.length) {
      output3 = output3.concat(days(iter) + ", ")
      iter += 1
    }
    println(output3)
  }

  //2a
  def daysOfTheWeekRec(days: List[String]): String = {
    if (days.isEmpty)
      ""
    else
      days.head + ", " + daysOfTheWeekRec(days.tail)
  }

  //2b
  def daysOfTheWeekRecReverse(days: List[String]): String = {
    val list = days.reverse
    if (list.isEmpty)
      ""
    else
      list.head + ", " + daysOfTheWeekRec(list.tail)
  }

  //3
  def daysOfTheWeekTailrec(days: List[String]): String = {
    @scala.annotation.tailrec
    def iter(tailrecDays: List[String], result: String): String = {
      if (tailrecDays.isEmpty)
        result
      else
        iter(tailrecDays.tail, result.concat(tailrecDays.head + ", "))
    }
    iter(days, "")
  }

  //4
  def daysOfTheWeekFold(days: List[String]) = {
    val output1 = days.foldLeft("")(_ + _ + ", ")
    println(output1)

    val output2 = days.foldRight("")(_ + ", " + _)
    println(output2)

    val output3 = days.foldRight("")((day, res) =>
      if (day.startsWith("P")) day + ", " + res else res
    )
    println(output3)
  }

  //5
  def makeDiscount(games: Map[String, Double]): Map[String, Double] = {
    games map { case (key, value) =>
      (key, (math rint value * 0.9 * 100) / 100)
    }
  }

  //6
  def printTuple(myTuple: Tuple3[String, Int, Double]) = {
    println(
      "Imie: " + myTuple._1 + ", Wiek: " + myTuple._2 + ", Wzrost: " + myTuple._3
    )
  }

  //7
  def sevenSummits(summits: Map[String, String]) = {
    println("get -> Asia: " + summits.get("Asia"))
    println("get -> Europe: " + summits.get("Europe"))
    println(
      "getOrElse -> North America: " + summits.getOrElse(
        "North America",
        "undefined"
      )
    )
    println(
      "getOrElse -> Australia: " + summits.getOrElse("Australia", "undefined")
    )
    println("isEmpty -> Australia: " + summits.get("Australia").isEmpty)
    println("isEmpty -> Africa: " + summits.get("Africa").isEmpty)
    println("isEmpty -> Summits: " + summits.isEmpty)
  }

  //8
  def removeZero(myList: List[Int]): List[Int] = myList match {
    case Nil => Nil
    case head :: tail =>
      if (head == 0)
        removeZero(tail)
      else
        head :: removeZero(tail)
  }

  //9
  def mapList(myList: List[Int]): List[Int] = {
    myList map (item => item + 1)
  }

  //10
  def filterList(myList: List[Int]): List[Int] = {
    myList.filter(x => x >= -5 && x <= 12) map (x => x.abs)
  }
}
