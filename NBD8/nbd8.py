import requests

url = 'http://ip172-18-0-54-c3c73n7njsv000fnk9ug-8098.direct.labs.play-with-docker.com/'

requests.post(url + 'buckets/moto/keys/moto1', {"name": "MT-07", "brand": "Yamaha", "cubicCapacity": 689, "damaged": False})
print('Dodano motocykl')

response = requests.get(url + 'buckets/moto/keys/moto1')
print('Dodany motocykl: ' + response.text)

requests.put(url + 'buckets/moto/keys/moto1', {"name": "MT-07", "brand": "Yamaha", "cubicCapacity": 689, "damaged": True})

response = requests.get(url + 'buckets/moto/keys/moto1')
print('Motocykl po modyfikacji: ' + response.text)

requests.delete(url + 'buckets/moto/keys/moto1')
print('Usunięto motocykl')
response = requests.get(url + 'buckets/moto/keys/moto1')
print('Pobieranie moto po usunięciu: ' + response.text)