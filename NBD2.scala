object Main {
  def main(args: Array[String]): Unit = {
    println("zad1")
    println("-------------------------")
    println(checkDay("Poniedziałek"))
    println(checkDay("Wtorek"))
    println(checkDay("Środa"))
    println(checkDay("Czwartek"))
    println(checkDay("Piątek"))
    println(checkDay("Sobota"))
    println(checkDay("Niedziela"))
    println(checkDay("Test"))
    println(checkDay("Szóstek"))

    println("\nzad2")
    println("-------------------------")
    val kb1 = new KontoBankowe()
    val kb2 = new KontoBankowe(1234)
    
    kb1.wplata(234)
    try{
      kb2.wyplata(200000)
    }catch {
      case _: Throwable => println("Za mało kasy na koncie")
    }
    
    try{
      kb2.wyplata(456)
    }catch {
      case _: Throwable => println("Za mało kasy na koncie")
    }
    
    println(kb1.stanKonta)
    println(kb2.stanKonta)

    println("\nzad3")
    println("-------------------------")
    val o1 = new Osoba("Anna", "Kuc")
    val o2 = new Osoba("Marek", "Lewarek")
    val o3 = new Osoba("Piotr", "Nowak")
    val o4 = new Osoba("Paweł", "Kowal")
    val o5 = new Osoba("Marta", "Makrela")

    println(przywitanie(o1))
    println(przywitanie(o2))
    println(przywitanie(o3))
    println(przywitanie(o4))
    println(przywitanie(o5))

    println("\nzad4")
    println("-------------------------")

    def funkcja(par: Int)(liczba: Int): Int = {
      if (liczba % 2 == 0) liczba / par
      else liczba / 2 * par
    }

    println(wywolaj3(1337, funkcja(3) _))

    println("\nzad5")
    println("-------------------------")

    val osoba1 = new Osoba2("Jan", "Nowak") with Student
    val osoba2 = new Osoba2("Anna", "Koc") with Pracownik
    osoba2.pensja = 1000
    val osoba3 = new Osoba2("Marcin", "Kowal") with Nauczyciel
    osoba3.pensja = 2137

    println(osoba1.toString())
    println(osoba2.toString())
    println(osoba3.toString())
    
    val osoba4 = new Osoba2("Ala", "Rogala") with Student with Nauczyciel
    val osoba5 = new Osoba2("Olga", "Kwoka") with Nauczyciel with Student
    
    osoba4.pensja = 2000
    osoba5.pensja = 2000
    
    println(osoba4.toString())
    println(osoba5.toString())
  }

  //1
  def checkDay(day: String): String = day match {
    case "Poniedziałek" => day + " -> " + "Praca"
    case "Wtorek"       => day + " -> " + "Praca"
    case "Środa"        => day + " -> " + "Praca"
    case "Czwartek"     => day + " -> " + "Praca"
    case "Piątek"       => day + " -> " + "Praca"
    case "Sobota"       => day + " -> " + "Weekend"
    case "Niedziela"    => day + " -> " + "Weekend"
    case _              => day + " -> " + "Nie ma takiego dnia"
  }

  //2
  class KontoBankowe(private var _stanKonta: Int) {

    def stanKonta = _stanKonta
    
    def this() = {
      this(0)
    }

    def wplata(kwota: Int) = {
      this._stanKonta = stanKonta + kwota;
    }

    def wyplata(kwota: Int) = {
      if (kwota < stanKonta)
        this._stanKonta = this.stanKonta - kwota;
      else
        throw new Exception()
    }
  }

  //3
  case class Osoba(imie: String, nazwisko: String)

  def przywitanie(osoba: Osoba): String = osoba match {
    case Osoba("Marek", "_")   => "Siema Marek!"
    case Osoba(_, "Nowak")     => "Kolejny Nowak?"
    case Osoba("Anna", _)      => "Witaj Aniu!"
    case Osoba(imie, nazwisko) => s"Dzień dobry, $imie $nazwisko"
  }

  //4
  def wywolaj3(liczba: Int, fun: Int => Int): Int = {
    fun(fun(fun(liczba)))
  }

  //5
  class Osoba2(private var _imie: String, private var _nazwisko: String) {
    private var _podatek: Double = 0
    
    def imie = _imie
    def nazwisko = _nazwisko
    def podatek = _podatek

    override def toString(): String = {
      s"Imie: $imie, Nazwisko: $nazwisko, Podatek: $podatek"
    }
  }

  trait Student extends Osoba2 {
    override def podatek = 0
  }

  trait Nauczyciel extends Pracownik {
    override def podatek = (math rint pensja * 0.1 * 100) / 100
  }

  trait Pracownik extends Osoba2 {
    private var _pensja = 0
    override def podatek = (math rint pensja * 0.2 * 100) / 100

    def pensja = _pensja
    def pensja_=(value: Int): Unit = _pensja = value

    override def toString(): String = {
      s"Imie: $imie, Nazwisko: $nazwisko, Podatek: $podatek, Pensja: $pensja"
    }
  }
}
